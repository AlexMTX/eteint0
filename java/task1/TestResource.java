package task1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/test")
public class TestResource {

	@GET
	@Produces(MediaType.APPLICATION_XML)
	public List<CottageListElement> getCottageList(
			@QueryParam("name") 			String name,
			@QueryParam("places") 			int places,
			@QueryParam("bedrooms") 		int bedrooms,
			@QueryParam("distanceToLake") 	int distanceToLake,
			@QueryParam("city") 			String city,
			@QueryParam("distanceToCity") 	int distanceToCity,
			@QueryParam("days") 			int days,
			@QueryParam("startingDate") 	String startingDate,
			@QueryParam("shift") 			int shift)
	{
		ArrayList<CottageListElement> list = new ArrayList<CottageListElement>();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
		if (startingDate!=null)
			try {
				date = sdf.parse(startingDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		gc.add(GregorianCalendar.DAY_OF_MONTH, days);
		
		CottageListElement element = new CottageListElement(name, 13, "Hell", "imageURL", places, bedrooms, distanceToLake, distanceToCity, city, date, gc.getTime());
		list.add(element);
		return list;
	}
	
}