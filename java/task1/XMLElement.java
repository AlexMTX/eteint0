package task1;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class XMLElement {

	private int number;
	
	private String text;
	
	private Date time;

	@XmlElement
	public int getNumber() {
		return number;
	}
	
	public void setNumber(int number) {
		this.number = number;
	}

	@XmlElement
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	@XmlElement
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public XMLElement() {
	}

	public XMLElement(int number, String text, Date time) {
		super();
		this.number = number;
		this.text = text;
		this.time = time;
	}
	
	
	
}
