package task1;

import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class XMLTester {

	public static void main(String[] args) throws JAXBException {
		CottageListElement cottageListElement = new CottageListElement();
		
		JAXBContext jaxbContext = JAXBContext.newInstance(CottageListElement.class);
		
		 Marshaller marshaller = jaxbContext.createMarshaller();
		 
		 marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		 
		 marshaller.marshal(cottageListElement, System.out);
	}

}
