package task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.Ontology;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.XSD;

public class Task {

	public static void main(String[] args) throws FileNotFoundException {
		
		OntModel ontModel = ModelFactory.createOntologyModel();
		String ontologyURI = "http://localhost:8080/CottageBookingOntology.owl";
		String ns = ontologyURI.concat("#");
		Ontology onotology = ontModel.createOntology(ontologyURI);
		
		OntClass Cottage = ontModel.createClass(ns+"Cottage");
		
		DatatypeProperty address = ontModel.createDatatypeProperty(ns+"address");
		address.setDomain(Cottage);
		address.setRange(XSD.xstring);
		
		DatatypeProperty image = ontModel.createDatatypeProperty(ns+"image");
		image.setDomain(Cottage);
		image.setRange(XSD.anyURI);
				
		DatatypeProperty amountOfPlaces = ontModel.createDatatypeProperty(ns+"amountOfPlaces");
		amountOfPlaces.setDomain(Cottage);
		amountOfPlaces.setRange(XSD.integer);
		
		DatatypeProperty amountOfBedrooms = ontModel.createDatatypeProperty(ns+"amountOfBedrooms");
		amountOfBedrooms.setDomain(Cottage);
		amountOfBedrooms.setRange(XSD.integer);
		
		DatatypeProperty distanceFromLake = ontModel.createDatatypeProperty(ns+"distanceFromLake");
		distanceFromLake.setDomain(Cottage);
		distanceFromLake.setRange(XSD.integer);
		
		DatatypeProperty nearesrCity = ontModel.createDatatypeProperty(ns+"nearesrCity");
		nearesrCity.setDomain(Cottage);
		nearesrCity.setRange(XSD.xstring);
		

		DatatypeProperty distanceToCity = ontModel.createDatatypeProperty(ns+"distanceToCity");
		distanceToCity.setDomain(Cottage);
		distanceToCity.setRange(XSD.integer);
		
		OntClass BookingPeriod = ontModel.createClass(ns+"BookingPeriod");
		
		DatatypeProperty amountOfDays = ontModel.createDatatypeProperty(ns+"amountOfDays");
		amountOfDays.setDomain(BookingPeriod);
		amountOfDays.setRange(XSD.date);

		DatatypeProperty startDate = ontModel.createDatatypeProperty(ns+"startDate");
		startDate.setDomain(BookingPeriod);
		startDate.setRange(XSD.date);
		
		DatatypeProperty endDate = ontModel.createDatatypeProperty(ns+"endDate");
		endDate.setDomain(BookingPeriod);
		endDate.setRange(XSD.date);
		
		OntClass Booking = ontModel.createClass(ns+"Booking");
		
		DatatypeProperty nameOfBooker = ontModel.createDatatypeProperty(ns+"nameOfBooker");
		nameOfBooker.setDomain(Booking);
		nameOfBooker.setRange(XSD.xstring);
				
		ObjectProperty bookingPeriod = ontModel.createObjectProperty(ns+"bookingPeriod");
		bookingPeriod.setDomain(Booking);
		bookingPeriod.setRange(BookingPeriod);
		
		ObjectProperty cottage = ontModel.createObjectProperty(ns+"cottage");
		cottage.setDomain(Booking);
		cottage.setRange(Cottage);
		
		Booking.addSubClass(BookingPeriod);
		
		DatatypeProperty shift = ontModel.createDatatypeProperty(ns+"shift");
		shift.setRange(XSD.date);
		
		File file= new File("CottageBookingOntology.owl");
		OutputStream out = new FileOutputStream(file);
		ontModel.write(out);
		
		Model model = ModelFactory.createDefaultModel();
		Resource cottageOne = model.createResource(ns+"CottageOne");
		Property cottageAddress = model.createProperty(ns+"address");
		Property cottageImageURL = model.createProperty(ns+"imageURL");
		Property cottageDistanceFromLake = model.createProperty(ns+"distanceFromLake");
		Property cottageNearestCity = model.createProperty(ns+"nearestCity");
		Property cottageDistanceToCity = model.createProperty(ns+"distanceToCity");
		Property cottageAmountOfPlaces = model.createProperty(ns+"amountOfPlaces");
		Property cottageAmountOfBedrooms = model.createProperty(ns+"amountOfBedrooms");	
		
		cottageOne.addProperty(cottageAddress, "Haity");
		cottageOne.addProperty(cottageImageURL, "http://localhost:8080/images/cottage.jpg");
		cottageOne.addProperty(cottageDistanceFromLake, "200");
		cottageOne.addProperty(cottageNearestCity, "Port-au-Prince");
		cottageOne.addProperty(cottageDistanceToCity, "3");
		cottageOne.addProperty(cottageAmountOfPlaces, "9");
		cottageOne.addProperty(cottageAmountOfBedrooms, "4");
		
		Resource cotaggeTwo = model.createResource(ns+"CottageTwo");
		cotaggeTwo.addProperty(cottageAddress, "Bahamas");
		cotaggeTwo.addProperty(cottageImageURL, "http://localhost:8080/images/cottage.jpg");
		cotaggeTwo.addProperty(cottageDistanceFromLake, "350");
		cotaggeTwo.addProperty(cottageNearestCity, "Nassau");
		cotaggeTwo.addProperty(cottageDistanceToCity, "12");
		cotaggeTwo.addProperty(cottageAmountOfPlaces, "11");
		cotaggeTwo.addProperty(cottageAmountOfBedrooms, "3");

		Resource cotaggeThree = model.createResource(ns+"CottageThree");
		cotaggeThree.addProperty(cottageAddress, "Cuba");
		cotaggeThree.addProperty(cottageImageURL, "http://localhost:8080/images/cottage.jpg");
		cotaggeThree.addProperty(cottageDistanceFromLake, "100");
		cotaggeThree.addProperty(cottageNearestCity, "Santiago de Cuba");
		cotaggeThree.addProperty(cottageDistanceToCity, "5");
		cotaggeThree.addProperty(cottageAmountOfPlaces, "25");
		cotaggeThree.addProperty(cottageAmountOfBedrooms, "7");
		
		Resource cotaggeFour = model.createResource(ns+"CottageFour");
		cotaggeFour.addProperty(cottageAddress, "Hawaii");
		cotaggeFour.addProperty(cottageImageURL, "http://localhost:8080/images/cottage.jpg");
		cotaggeFour.addProperty(cottageDistanceFromLake, "537");
		cotaggeFour.addProperty(cottageNearestCity, "Honolulu");
		cotaggeFour.addProperty(cottageDistanceToCity, "7");
		cotaggeFour.addProperty(cottageAmountOfPlaces, "10");
		cotaggeFour.addProperty(cottageAmountOfBedrooms, "4");
		
		Resource cotaggeFive = model.createResource(ns+"CottageFive");
		cotaggeFive.addProperty(cottageAddress, "Goa");
		cotaggeFive.addProperty(cottageImageURL, "http://localhost:8080/images/cottage.jpg");
		cotaggeFive.addProperty(cottageDistanceFromLake, "75");
		cotaggeFive.addProperty(cottageNearestCity, "Vasco da Gama");
		cotaggeFive.addProperty(cottageDistanceToCity, "13");
		cotaggeFive.addProperty(cottageAmountOfPlaces, "17");
		cotaggeFive.addProperty(cottageAmountOfBedrooms, "6");
		
		File rdfDatabase = new File("cottageDB.rdf");
		OutputStream rdf = new FileOutputStream(rdfDatabase);
		model.write(rdf, "N3");
	}

}
