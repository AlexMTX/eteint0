package task1;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CottageListElement {
/*
		name of the booker;
		booking number;
		address of the cottage;
		image of the cottage (URL of the image in the web);
		actual amount of places in the cottage;
		actual amount of bedrooms in the cottage;
		actual distance to the lake (meters);
		nearest city and a distance to it from the cottage;
		booking period.   
*/
	private String nameOfBooker;
	private int bookingNumber;
	private String cottageAddress;
	private String imageURL;
	private int amountOfPlaces;
	private int amountOfBedrooms;
	private int distanceToLake;
	private int distanceToCity;
	private String nearestCity;
	private Date periodStart;
	private Date periodEnd;
	
	@XmlElement
	public String getNameOfBooker() {
		return nameOfBooker;
	}
	public void setNameOfBooker(String nameOfBooker) {
		this.nameOfBooker = nameOfBooker;
	}
	@XmlElement
	public int getBookingNumber() {
		return bookingNumber;
	}
	public void setBookingNumber(int bookingNumber) {
		this.bookingNumber = bookingNumber;
	}
	@XmlElement
	public String getCottageAddress() {
		return cottageAddress;
	}
	public void setCottageAddress(String cottageAddress) {
		this.cottageAddress = cottageAddress;
	}
	@XmlElement
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	@XmlElement
	public int getAmountOfPlaces() {
		return amountOfPlaces;
	}
	public void setAmountOfPlaces(int amountOfPlaces) {
		this.amountOfPlaces = amountOfPlaces;
	}
	@XmlElement
	public int getAmountOfBedrooms() {
		return amountOfBedrooms;
	}
	public void setAmountOfBedrooms(int amountOfBedrooms) {
		this.amountOfBedrooms = amountOfBedrooms;
	}
	@XmlElement
	public int getDistanceToLake() {
		return distanceToLake;
	}
	public void setDistanceToLake(int distanceToLake) {
		this.distanceToLake = distanceToLake;
	}
	@XmlElement
	public int getDistanceToCity() {
		return distanceToCity;
	}
	public void setDistanceToCity(int distanceToCity) {
		this.distanceToCity = distanceToCity;
	}
	@XmlElement
	public String getNearestCity() {
		return nearestCity;
	}
	public void setNearestCity(String nearestCity) {
		this.nearestCity = nearestCity;
	}
	@XmlElement
	public Date getPeriodStart() {
		return periodStart;
	}
	public void setPeriodStart(Date periodStart) {
		this.periodStart = periodStart;
	}
	@XmlElement
	public Date getPeriodEnd() {
		return periodEnd;
	}
	public void setPeriodEnd(Date periodEnd) {
		this.periodEnd = periodEnd;
	}
	
	public CottageListElement(String nameOfBooker, int bookingNumber,
			String cottageAddress, String imageURL, int amountOfPlaces,
			int amountOfBedrooms, int distanceToLake, int distanceToCity,
			String nearestCity, Date periodStart, Date periodEnd) {
		super();
		this.nameOfBooker = nameOfBooker;
		this.bookingNumber = bookingNumber;
		this.cottageAddress = cottageAddress;
		this.imageURL = imageURL;
		this.amountOfPlaces = amountOfPlaces;
		this.amountOfBedrooms = amountOfBedrooms;
		this.distanceToLake = distanceToLake;
		this.distanceToCity = distanceToCity;
		this.nearestCity = nearestCity;
		this.periodStart = periodStart;
		this.periodEnd = periodEnd;
	}
	
	public CottageListElement() {
	}
	
	@Override
	public String toString() {
		return "CottageListElement [nameOfBooker=" + nameOfBooker
				+ ", bookingNumber=" + bookingNumber + ", cottageAddress="
				+ cottageAddress + ", imageURL=" + imageURL
				+ ", amountOfPlaces=" + amountOfPlaces + ", amountOfBedrooms="
				+ amountOfBedrooms + ", distanceToLake=" + distanceToLake
				+ ", distanceToCity=" + distanceToCity + ", nearestCity="
				+ nearestCity + ", periodStart=" + periodStart + ", periodEnd="
				+ periodEnd + "]";
	}
	
	
	
}
