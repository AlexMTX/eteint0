package rest;

import java.util.Date;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import task1.XMLElement;

@Path("/xmlelements")
public class XMLElementEndpoint {

	@GET
	@Produces("application/xml")
	public XMLElement xmlElements() {
		XMLElement element = new XMLElement(13,"E1-Diab1o", new Date());
		
		
		return element;
	}

}
