package task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.Ontology;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.XSD;

public class Task {

	public static void main(String[] args) throws FileNotFoundException {
		
		OntModel ontModel = ModelFactory.createOntologyModel();
		String ontologyURI = "http://localhost:8080/CottageBookingOntology.owl";
		String ns = ontologyURI.concat("#");
		Ontology onotology = ontModel.createOntology(ontologyURI);
		
		OntClass Cottage = ontModel.createClass(ns+"Cottage");
		
		DatatypeProperty address = ontModel.createDatatypeProperty(ns+"address");
		address.setDomain(Cottage);
		address.setRange(XSD.xstring);
		
		DatatypeProperty image = ontModel.createDatatypeProperty(ns+"image");
		image.setDomain(Cottage);
		image.setRange(XSD.anyURI);
				
		DatatypeProperty amountOfPlaces = ontModel.createDatatypeProperty(ns+"amountOfPlaces");
		amountOfPlaces.setDomain(Cottage);
		amountOfPlaces.setRange(XSD.integer);
		
		DatatypeProperty amountOfBedrooms = ontModel.createDatatypeProperty(ns+"amountOfBedrooms");
		amountOfBedrooms.setDomain(Cottage);
		amountOfBedrooms.setRange(XSD.integer);
		
		DatatypeProperty distanceFromLake = ontModel.createDatatypeProperty(ns+"distanceFromLake");
		distanceFromLake.setDomain(Cottage);
		distanceFromLake.setRange(XSD.integer);
		
		DatatypeProperty nearesrCity = ontModel.createDatatypeProperty(ns+"nearesrCity");
		nearesrCity.setDomain(Cottage);
		nearesrCity.setRange(XSD.xstring);
		

		DatatypeProperty distanceToCity = ontModel.createDatatypeProperty(ns+"distanceToCity");
		distanceToCity.setDomain(Cottage);
		distanceToCity.setRange(XSD.integer);
		
		OntClass BookingPeriod = ontModel.createClass(ns+"BookingPeriod");
		
		DatatypeProperty amountOfDays = ontModel.createDatatypeProperty(ns+"amountOfDays");
		amountOfDays.setDomain(BookingPeriod);
		amountOfDays.setRange(XSD.date);

		DatatypeProperty startDate = ontModel.createDatatypeProperty(ns+"startDate");
		startDate.setDomain(BookingPeriod);
		startDate.setRange(XSD.date);
		
		DatatypeProperty endDate = ontModel.createDatatypeProperty(ns+"endDate");
		endDate.setDomain(BookingPeriod);
		endDate.setRange(XSD.date);
		
		OntClass Booking = ontModel.createClass(ns+"Booking");
		
		DatatypeProperty nameOfBooker = ontModel.createDatatypeProperty(ns+"nameOfBooker");
		nameOfBooker.setDomain(Booking);
		nameOfBooker.setRange(XSD.xstring);
				
		ObjectProperty bookingPeriod = ontModel.createObjectProperty(ns+"bookingPeriod");
		bookingPeriod.setDomain(Booking);
		bookingPeriod.setRange(BookingPeriod);
		
		ObjectProperty cottage = ontModel.createObjectProperty(ns+"cottage");
		cottage.setDomain(Booking);
		cottage.setRange(Cottage);
		
		Booking.addSubClass(BookingPeriod);
		
		DatatypeProperty shift = ontModel.createDatatypeProperty(ns+"shift");
		shift.setRange(XSD.date);
		
		File file= new File("CottageBookingOntology.owl");
		OutputStream out = new FileOutputStream(file);
		ontModel.write(out);
		
		Model model = ModelFactory.createDefaultModel();

		Property cottageAddress = model.createProperty(ns+"address");
		Property cottageImageURL = model.createProperty(ns+"imageURL");
		Property cottageDistanceFromLake = model.createProperty(ns+"distanceFromLake");
		Property cottageNearestCity = model.createProperty(ns+"nearestCity");
		Property cottageDistanceToCity = model.createProperty(ns+"distanceToCity");
		Property cottageAmountOfPlaces = model.createProperty(ns+"amountOfPlaces");
		Property cottageAmountOfBedrooms = model.createProperty(ns+"amountOfBedrooms");	
		Property aCottage = model.createProperty(ns, "isA");
		
		Resource cottageOne = model.createResource("CottageOne");
		cottageOne.addProperty(cottageAddress, "Haity");
		cottageOne.addProperty(cottageImageURL, "http://localhost:8080/images/cottageOne.jpg");
		cottageOne.addProperty(cottageDistanceFromLake, "200");
		cottageOne.addProperty(cottageNearestCity, "Port-au-Prince");
		cottageOne.addProperty(cottageDistanceToCity, "3");
		cottageOne.addProperty(cottageAmountOfPlaces, "9");
		cottageOne.addProperty(cottageAmountOfBedrooms, "4");
		Statement cottageOneIsACottage = model.createStatement(cottageOne, aCottage, Cottage);
		model.add(cottageOneIsACottage);
		
		Resource cottageTwo = model.createResource("CottageTwo");
		cottageTwo.addProperty(cottageAddress, "Bahamas");
		cottageTwo.addProperty(cottageImageURL, "http://localhost:8080/images/cottageTwo.jpg");
		cottageTwo.addProperty(cottageDistanceFromLake, "350");
		cottageTwo.addProperty(cottageNearestCity, "Nassau");
		cottageTwo.addProperty(cottageDistanceToCity, "12");
		cottageTwo.addProperty(cottageAmountOfPlaces, "11");
		cottageTwo.addProperty(cottageAmountOfBedrooms, "3");
		Statement cottageTwoIsACottage = model.createStatement(cottageTwo, aCottage, Cottage);
		model.add(cottageTwoIsACottage);

		Resource cottageThree = model.createResource("CottageThree");
		cottageThree.addProperty(cottageAddress, "Cuba");
		cottageThree.addProperty(cottageImageURL, "http://localhost:8080/images/cottageThree.jpg");
		cottageThree.addProperty(cottageDistanceFromLake, "100");
		cottageThree.addProperty(cottageNearestCity, "Santiago de Cuba");
		cottageThree.addProperty(cottageDistanceToCity, "5");
		cottageThree.addProperty(cottageAmountOfPlaces, "25");
		cottageThree.addProperty(cottageAmountOfBedrooms, "7");
		Statement cottageThreeIsACottage = model.createStatement(cottageThree, aCottage, Cottage);
		model.add(cottageThreeIsACottage);
		
		Resource cottageFour = model.createResource("CottageFour");
		cottageFour.addProperty(cottageAddress, "Hawaii");
		cottageFour.addProperty(cottageImageURL, "http://localhost:8080/images/cottageFour.jpg");
		cottageFour.addProperty(cottageDistanceFromLake, "537");
		cottageFour.addProperty(cottageNearestCity, "Honolulu");
		cottageFour.addProperty(cottageDistanceToCity, "7");
		cottageFour.addProperty(cottageAmountOfPlaces, "10");
		cottageFour.addProperty(cottageAmountOfBedrooms, "4");
		Statement cottageFourIsACottage = model.createStatement(cottageFour, aCottage, Cottage);
		model.add(cottageFourIsACottage);
		
		Resource cottageFive = model.createResource("CottageFive");
		cottageFive.addProperty(cottageAddress, "Goa");
		cottageFive.addProperty(cottageImageURL, "http://localhost:8080/images/cottageFive.jpg");
		cottageFive.addProperty(cottageDistanceFromLake, "75");
		cottageFive.addProperty(cottageNearestCity, "Vasco da Gama");
		cottageFive.addProperty(cottageDistanceToCity, "13");
		cottageFive.addProperty(cottageAmountOfPlaces, "17");
		cottageFive.addProperty(cottageAmountOfBedrooms, "6");
		Statement cottageFiveIsACottage = model.createStatement(cottageFive, aCottage, Cottage);
		model.add(cottageFiveIsACottage);
		
		model.setNsPrefix("ns", ns);
		File rdfDatabase = new File("cottageDB.rdf");
		OutputStream rdf = new FileOutputStream(rdfDatabase);
		model.write(rdf, "TURTLE");
	}

}
